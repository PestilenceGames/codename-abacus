# Codename Abacus

Luck based modular framework.

This project was designed to be a simple blackjack system that could be use in other games. 

It follows the Las Vegas Blackjack rules and conventions.

Controls:
START - Begins a new game
RESET - Clears the current hand with no win/loss
BET - Raise or lower the current hands value
HIT - request another card from the dealer
STAND - Turn control over to the dealer
DOUBLE - Double the betting value of the hand (when compliant with certain rules)
SPLIT - Creates two hands from the original one (when compliant with certain rules)

The aim of the game is to get as close to 21 without going over (bust) while competing with the dealer.

*Note* The game has no main menu or pause, as the game was designed to be an expandable sytem to add to another framework. Force Quit is required.
