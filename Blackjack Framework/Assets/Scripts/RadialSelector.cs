﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RadialSelector : MonoBehaviour
{
    [SerializeField] private GameManager gM02;
    [SerializeField] private Player player;
    [SerializeField] private int selectionType;
    [SerializeField] private Vector3 restScale;
    [SerializeField] private Vector3 hoverScale;
    private void Awake()
    {
        restScale = new Vector3(0.8f, 0.8f, 0.8f);
        hoverScale = new Vector3(1, 1, 1);

        this.transform.localScale = Vector3.zero;
        StartCoroutine(Grow());
    }

    private void Update()
    {
        if (gM02.CheckIfObjectIsHovered(this.gameObject))
        {
            this.transform.localScale = Vector3.Lerp(this.transform.localScale, hoverScale, 0.3f);

            if (selectionType == 0)
                player.ShowHandValue();
        }
        else
            this.transform.localScale = Vector3.Lerp(this.transform.localScale, restScale, 0.3f);

    }

    IEnumerator Grow()
    {
        for (var i = 0; i < 200; i++)
        {
            this.transform.localScale = Vector3.Lerp(this.transform.localScale, restScale, 0.1f);
            yield return null;
        }
    }

    public void Click()
    {
        switch (selectionType)
        {
            case 0:
                break;
            case 1:
                player.Hit();
                break;
            case 2:
                player.Stand();
                break;
            case 3:
                player.Double();
                break;
            case 4:
                player.Split();
                break;
            default:
                break;
        }
        
    }

}
