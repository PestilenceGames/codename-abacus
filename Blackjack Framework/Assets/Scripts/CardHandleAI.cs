﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardHandleAI : MonoBehaviour
{
    [SerializeField] private UIManager uI;
    [SerializeField] private List<GameObject> suits = new List<GameObject>();
    [SerializeField] private List<GameObject> faces = new List<GameObject>();
    public Vector3Int thisCardValue;
    [SerializeField] private bool flipped;
    [SerializeField] private bool changableAce = true; //stops player from changing dealer's aces

    private void Awake()
    {
        this.transform.localScale = Vector3.zero;
        StartCoroutine(Grow());
    }

    public void ClearCard()
    {
        foreach (GameObject _suit in suits)
        {
            _suit.SetActive(false);
        }

        foreach (GameObject _face in faces)
        {
            _face.SetActive(false);
        }
    }
    public Vector3Int DrawRandomCard(Vector3 _position, bool _flipped) //Position is redundant and unused, keep in mind
    {
        ClearCard();

        if (!_flipped)
            this.transform.rotation = Quaternion.Euler(0f, 0f, 0f);
        else
        {
            this.transform.rotation = Quaternion.Euler(0f, 0f, 180f);
            this.transform.GetComponent<SphereCollider>().enabled = false;
        }

        int _faceValue = Random.Range(0, 13); //Face of Card
        int _suitValue = Random.Range(0, 4); //Suit of Card

        int _value = CalculateValue(_faceValue);

        thisCardValue = new Vector3Int(_faceValue, _suitValue, _value);

        suits[_suitValue].SetActive(true);
        faces[_faceValue].SetActive(true);

        return new Vector3Int(_faceValue, _suitValue, _value);
    }
    public int CalculateValue(int _face)
    {
        if (_face >= 0 && _face <= 9) //Ace to 10
        {
            return _face + 1;
        }
        else if (_face >= 10)
        {
            return 10;
        }
        else
        {
            return 3;
        }
    }

    public void FlipAround()
    {
        flipped = true;
        this.transform.rotation = Quaternion.Euler(Vector3.zero);
        //Good place to add flipping animations       
    }

    IEnumerator Grow() //Deal animations
    {
        for (var i = 0; i < 200; i++)
        {
            this.transform.localScale = Vector3.Lerp(this.transform.localScale, Vector3.one, 0.1f);
            yield return null;
        }
    }

    public Vector3Int DrawSpecificCard(Vector3Int _value)
    {
        ClearCard();
        thisCardValue = new Vector3Int(_value.x, _value.y, _value.z);
        suits[_value.y].SetActive(true);
        faces[_value.x].SetActive(true);

        return thisCardValue;
    }

    public Vector3Int GetCardVector()
    {
        return thisCardValue;
    }

    public void DisplayCard()
    {
        uI.DisplayTextNearMouse($"Card Value {thisCardValue.z}");
    }

    public void ChangeAce()
    {
        if (changableAce)
        {
            Debug.Log("Change Ace");
            if (thisCardValue.x == 0)
            {
                switch (thisCardValue.z)
                {
                    case 1:
                        thisCardValue.z = 10;
                        break;
                    case 10:
                        thisCardValue.z = 1;
                        break;
                    default:
                        break;
                }
            }
        }
    }

    public void DisableAceChanging()
    {
        changableAce = false;
    }

}


