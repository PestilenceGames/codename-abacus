﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    [Header("Script References")]
    [SerializeField] private Dealer dealer;
    [SerializeField] private UIManager interfaceManager;

    [Header("UI Settings")]
    [SerializeField] private Camera mainCamera;
    [SerializeField] private LayerMask clickable;
    [SerializeField] private GameObject card;

    [Header("Player Settings")]
    [SerializeField] private List<GameObject> players = new List<GameObject>();
    [SerializeField] private GameObject player;
    [SerializeField] private Vector3 playerOffset;

    private Ray intoWorld;
    private RaycastHit hit;
    private GameObject target;
    private GameObject newCard;
    private GameObject newPlayer;
    private RadialSelector radial;
    private Player playerScript;
    private bool onHover;
    void Start()
    {
        player.SetActive(false);
        players.Clear();
        // players.Add(Instantiate(player));
        Reset();
    }

    public void Reset()
    {
        Debug.Log("Reset");
        if (players.Count > 0)
        {
            foreach (GameObject _player in players)
            {
                _player.GetComponent<Player>().Reset();
            }

            foreach (GameObject _player in players) //Done Twice for Sequence
            {
                Destroy(_player);
            }

            // Destroy(players[0]);
            players.Clear();
        }

        dealer.DealerReset();
        interfaceManager.Reset();
        // players[0].GetComponent<Player>().Reset();
    }
    void Update() //Used to active the radial
    {
        intoWorld = mainCamera.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(intoWorld, out hit, 2000f, clickable))
        {
            target = hit.transform.gameObject;
            radial = target.transform.GetComponent<RadialSelector>();
            if (target.transform.CompareTag("Card"))
            {
                target.transform.GetComponent<CardHandleAI>().DisplayCard();
            }
        }
        else
            target = null;
        if (Input.GetMouseButtonDown(0))
        {
            intoWorld = mainCamera.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(intoWorld, out hit, 2000f, clickable))
            {
                if (target.CompareTag("Card"))
                {
                    hit.transform.GetComponent<CardHandleAI>().ChangeAce();
                    ReEvaluate();
                }
                else
                {
                    target = hit.transform.gameObject;
                    target.transform.GetComponent<RadialSelector>().Click();
                }
            }
        }
    }

    public void StartGame()
    {
        Debug.Log("Start");
        StartCoroutine(dealer.Deal());
    }

    public void AddPlayer(bool _random, Vector3Int _card)
    {
        //trying to elimainate repetition
        int _count = players.Count;

        newPlayer = Instantiate(player, player.transform.position + playerOffset * (_count), player.transform.rotation);
        players.Add(newPlayer);
        playerScript = newPlayer.GetComponent<Player>();
        newPlayer.SetActive(true);
        playerScript.PlayerDraw(_random, _card);


    }

    public bool CheckIfObjectIsHovered(GameObject _radialWedge)
    {
        onHover = false;
        if (target != null && target.transform.gameObject == _radialWedge)
            onHover = true;
        return onHover;
    }

    public GameObject DrawACard(Vector3 _cardPos, bool _flipped)
    {
        newCard = Instantiate(card, _cardPos, card.transform.rotation);
        newCard.GetComponent<CardHandleAI>().DrawRandomCard(_cardPos, _flipped);

        return newCard;
    }

    public GameObject DrawSpecificCard(Vector3 _cardPos, bool _flipped, Vector3Int _value)
    {
        newCard = Instantiate(card, _cardPos, card.transform.rotation);
        newCard.GetComponent<CardHandleAI>().DrawSpecificCard(_value);

        return newCard;
    }

    public int FindCardValue(GameObject _card)
    {
        int _value = _card.GetComponent<CardHandleAI>().thisCardValue.z;
        return _value;
    }

    public int FindPipValue(GameObject _card)
    {
        int _value = _card.GetComponent<CardHandleAI>().thisCardValue.x;
        return _value;
    }

    public void Bust()
    {
        int _counter = 0;
        int _stood = 0;

        foreach (GameObject _player in players)
        {
            if (_player.GetComponent<Player>().IsActive())
            {
                _counter++;
                if (_player.GetComponent<Player>().HasStood())
                {
                    _stood++;
                }
            }
        }

        if (_counter == _stood && _counter != 0) //All still active have stood
        {
            StartCoroutine(dealer.FlipCard());
            StartCoroutine(dealer.DealTo17());
            // Deal to 17
        }
        else if (_counter == 0)
        {
            Reset();
        }
    }

    public void Stand()
    {
        int _counter = 0;
        int _stood = 0;

        foreach (GameObject _player in players)
        {
            if (_player.GetComponent<Player>().IsActive())
            {
                _counter++;
                if (_player.GetComponent<Player>().HasStood())
                {
                    _stood++;
                }
            }
        }

        if (_counter == _stood) //All still active have stood
        {
            StartCoroutine(dealer.FlipCard());
            StartCoroutine(dealer.DealTo17());
            // Deal to 17
        }

    }

    public void EvaluateAllHands(int _dealersHand)
    {
        foreach (GameObject _player in players)
        {
            playerScript = _player.GetComponent<Player>();
            if (playerScript.HasStood())
            {
                switch (CompareHands(_dealersHand, playerScript.GetPlayersHand()))

                {
                    case 0:
                        interfaceManager.Win();
                        Debug.Log($"Player wins with {playerScript.GetPlayersHand()} over {_dealersHand}");
                        interfaceManager.DisplayTextNearMouse($"Player wins with {playerScript.GetPlayersHand()} over {_dealersHand}");
                        break;
                    case 1:
                        interfaceManager.Lose();
                        Debug.Log($"Player loses with {playerScript.GetPlayersHand()} over {_dealersHand}");
                        interfaceManager.DisplayTextNearMouse($"Player loses with {playerScript.GetPlayersHand()} over {_dealersHand}");
                        break;
                    case 2:
                        Debug.Log($"Player ties with {playerScript.GetPlayersHand()} over {_dealersHand}");
                        interfaceManager.DisplayTextNearMouse($"Player ties with {playerScript.GetPlayersHand()} over {_dealersHand}");
                        break;
                    case 3:
                        Debug.Log($"Dealer Busts with {_dealersHand}");
                        interfaceManager.DisplayTextNearMouse($"Dealer Busts with {_dealersHand}");
                        interfaceManager.Win();
                        break;
                    default:
                        break;
                }
            }
            playerScript.Active(false);
        }

        Debug.Log("Evaluate");
        Reset();

    }

    private int CompareHands(int _hand01, int _hand02)
    {
        int _comparisonValue = 0;

        if (_hand01 < _hand02) //Win
            _comparisonValue = 0;
        else if (_hand01 > _hand02) //Lose
            _comparisonValue = 1;
        else if (_hand01 == _hand02)// Tie
            _comparisonValue = 2;

        if (_hand01 > 21)
            _comparisonValue = 3;
        Debug.Log($"hands Compared with a {_comparisonValue} outcome");
        return _comparisonValue;
    }

    public void ReEvaluate()
    {
        foreach (GameObject _player in players)
        {
            if (_player.GetComponent<Player>().IsActive())
            {
                _player.GetComponent<Player>().ReEvaluate();
            }
        }
    }

    public int GetPlayerCount()
    {
        return players.Count;
    }



}
