﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class UIManager : MonoBehaviour
{
    [Header("Scripts")]
    [SerializeField] private GameManager gM;

    [Header("Text")]
    [SerializeField] TMP_Text chipCountText;
    [SerializeField] TMP_Text betAmountText;
    [SerializeField] TMP_Text warningText;
    [SerializeField] TMP_Text dealersHandValueText;
    [SerializeField] TMP_Text mousePointerText;

    [Header("Buttons")]
    [SerializeField] List<GameObject> buttons = new List<GameObject>();
    public GameObject startButton;
    public GameObject restartButton;
    public GameObject raiseButton;
    public GameObject lowerButton;

    [Header("Betting")]
    [SerializeField] private int playerChips;
    [SerializeField] private int betAmount;
    [SerializeField] private int betIncrement;
    [SerializeField] private int minBet;

    // Internal


    private void Start() //HouseKeeping
    {
        warningText.CrossFadeAlpha(0f, 0f, false);
        UpdateDealerHand(0);
        // TakeBetFromChips();
        UpdatePlayerChips();
        UpdateBetAmount(betAmount);
        mousePointerText.text = "";
    }


    public void StartGame()
    {
        foreach (GameObject _button in buttons)
        {
            _button.SetActive(false);
        }
        UpdatePlayerChips();
    }

    public void RaiseBet()
    {
        if (playerChips - betIncrement >= 0)
        {
            playerChips = playerChips - betIncrement;
            UpdatePlayerChips();
            betAmount = betAmount + betIncrement;
            UpdateBetAmount(betAmount);
        }
        else
        {
            DisplayWarning($"You can't afford that bet.", 1.5f);
        }

        print("Raise");
    }

    public void LowerBet()
    {
        if (betAmount - betIncrement >= minBet)
        {
            playerChips = playerChips + betIncrement;
            UpdatePlayerChips();
            betAmount = betAmount - betIncrement;
            UpdateBetAmount(betAmount);
        }
        else
        {
            DisplayWarning($"You must bet at least {minBet}", 1.5f);
        }
        print("Lower");
    }

    public void UpdateBetAmount(int _bet)
    {
        betAmountText.text = $"Player Bet: {_bet}";
    }

    public void UpdatePlayerChips()
    {
        chipCountText.text = $"Player Chips: {playerChips}";
    }


    public void DisplayWarning(string _message, float _time)
    {
        warningText.text = _message;
        warningText.CrossFadeAlpha(1.0f, 0f, false);
        warningText.CrossFadeAlpha(0f, _time, false);
    }

    public void UpdateDealerHand(int _dealersHandValue)
    {
        dealersHandValueText.text = $"Dealer's hand: {_dealersHandValue} ";
    }

    public void DisplayTextNearMouse(string _display)
    {
        mousePointerText.text = _display;
        mousePointerText.CrossFadeAlpha(1f, 0f, false);
        mousePointerText.CrossFadeAlpha(0f, 2f, false);

        // mousePointerText.transform.position = Input.mousePosition;

    }

    public void Bust()
    {
        playerChips -= betAmount;
        UpdatePlayerChips();
    }

    public void Split()
    {
        playerChips -= betAmount;
        UpdatePlayerChips();
    }

    public void Reset()
    {
        // betAmount = 0;
        foreach (GameObject _button in buttons)
        {
            _button.SetActive(true);
        }
        TakeBetFromChips();
    }

    public void Double()
    {
        playerChips -= betAmount;
        UpdatePlayerChips();
        betAmount *= 2;
        UpdateBetAmount(betAmount);
    }

    public void Win()
    {
        playerChips += betAmount * 2;
        UpdatePlayerChips();
    }
    public void Lose()
    {
        playerChips -= betAmount;
        UpdatePlayerChips();
    }
    public void BlackJack()
    {
        playerChips += betAmount + betAmount / 2;
        UpdatePlayerChips();
    }

    private void TakeBetFromChips()
    {
        if (playerChips < minBet)
        {
            DisplayWarning($"You have lost too many chips. Chips reset for testing", 4f);
            playerChips = 100;
        }

        if (betAmount != minBet)
        {
            betAmount = minBet;
            playerChips -= betAmount;
        }

        UpdatePlayerChips();
        UpdateBetAmount(betAmount);
    }


    public int GetBet()
    {
        return betAmount;
    }


}
