﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    [Header("Scripts")]
    [SerializeField] private GameManager gM;
    [SerializeField] private UIManager interfaceManager;

    [Header("Player Objects")]
    [SerializeField] private GameObject selectionLight;
    [SerializeField] private List<GameObject> buttons = new List<GameObject>();

    [Header("Player settings")]
    [SerializeField] private Vector3 playerDrawOffset;
    [SerializeField] private float timeBetweenDraws;
    [SerializeField] private List<GameObject> playerCards = new List<GameObject>();
    private GameObject currentCard;
    private Vector3 newSpawnPos;
    private bool bust;
    private bool active;
    private bool stand;
    private bool doubled;
    [SerializeField] private int playerHand;

    private void Awake()
    {
        Reset();
    }

    public void Reset()
    {
        SelectionLightToggle(false);
        if (playerCards.Count > 0)
        {
            foreach (GameObject _card in playerCards)
            {
                Destroy(_card);
            }
            playerCards.Clear();
        }

        DisableRadialButtons();

        playerHand = 0;
        newSpawnPos = this.transform.position + playerDrawOffset;
        // this.gameObject.SetActive(false);
    }

    public void SelectionLightToggle(bool _state)
    {
        selectionLight.SetActive(_state);
    }

    public void PlayerDraw(bool _random, Vector3Int _value)
    {
        if (_random)
            StartCoroutine(Draw(2));
        else
            StartCoroutine(DrawDuplicate(newSpawnPos, _value));
        active = true;
        bust = false;
        stand = false;
        doubled = false;
    }

    private IEnumerator Draw(int _count)
    {
        for (var i = 0; i < _count; i++)
        {
            yield return new WaitForSeconds(timeBetweenDraws);
            currentCard = gM.DrawACard(newSpawnPos, false);
            newSpawnPos = currentCard.transform.position + playerDrawOffset;
            playerCards.Add(currentCard);
            playerHand += gM.FindCardValue(currentCard);
        }

        EvaluatePlayerHand();
    }

    private IEnumerator DrawDuplicate(Vector3 _Pos, Vector3Int _duplicateValue)
    {
        yield return new WaitForSeconds(timeBetweenDraws);
        currentCard = gM.DrawSpecificCard(_Pos, false, _duplicateValue);
        playerCards.Clear();
        playerCards.Add(currentCard);
        playerHand = gM.FindCardValue(currentCard);


        newSpawnPos += playerDrawOffset;
        StartCoroutine(Draw(1));

    }

    private void EvaluatePlayerHand()
    {
        if (playerCards.Count == 2) //First Round
        {
            int _card01Pip = gM.FindPipValue(playerCards[0]);
            int _card02Pip = gM.FindPipValue(playerCards[1]);

            if (_card01Pip == 0 && gM.FindCardValue(playerCards[1]) == 10 || _card02Pip == 0 && gM.FindCardValue(playerCards[0]) == 10)
            {
                Debug.Log("BlackJack");
                BlackJack();
            }
            else
            {

                if (_card01Pip == _card02Pip && (gM.GetPlayerCount() < 3)) //Allows Splitting up to 3 hands
                    buttons[3].SetActive(true);
                if (playerHand >= 9 && playerHand <= 11 && !doubled) //Allow Double
                    buttons[2].SetActive(true);

                //buttons[3].SetActive(true); //Force Split for debug, uncomment this if to split all, for testing.
            }
        }


        if (playerHand > 21)
        {
            Debug.Log("This Hand Busts");
            bust = true;
            active = false;
            interfaceManager.DisplayTextNearMouse($"This hand Busts with a total of {playerHand}");
            gM.Bust();
            interfaceManager.Bust();
        }
        else
        {
            for (int i = 0; i < 2; i++)
            {
                buttons[i].SetActive(true);
            }
        }
    }

    public void ShowHandValue()
    {
        interfaceManager.DisplayTextNearMouse($"This hand is worth {interfaceManager.GetBet()} with a value of {playerHand}");
    }

    public void Hit()
    {
        DisableRadialButtons();
        StartCoroutine(Draw(1));
    }
    public void Stand()
    {
        DisableRadialButtons();
        stand = true;
        gM.Stand();
    }
    public void Double()
    {
        DisableRadialButtons();
        interfaceManager.Double();
        doubled = true;
        EvaluatePlayerHand();
    }
    public void Split()
    {
        Debug.Log("Split");
        DisableRadialButtons();
        gM.AddPlayer(false, playerCards[1].GetComponent<CardHandleAI>().GetCardVector());
        playerHand -= gM.FindCardValue(playerCards[1]);
        Destroy(playerCards[1]);
        playerCards.RemoveAt(1);
        newSpawnPos -= playerDrawOffset;
        StartCoroutine(Draw(1));
        interfaceManager.Split();
    }

    public void DisableRadialButtons()
    {
        foreach (GameObject _button in buttons)
        {
            _button.SetActive(false);
        }
    }

    public void Active(bool _active)
    {
        active = _active;
    }

    public bool IsActive()
    {
        return active;
    }

    public bool HasStood()
    {
        return stand;
    }

    public int GetPlayersHand()
    {
        return playerHand;
    }

    public void BlackJack()
    {
        interfaceManager.DisplayTextNearMouse("Blackjack");
        active = false;
        interfaceManager.BlackJack();
        gM.Bust();
    }

    public void ReEvaluate()
    {
        playerHand = 0;
        foreach (GameObject _card in playerCards)
        {
            playerHand += gM.FindCardValue(_card);
        }
        EvaluatePlayerHand();
    }

}