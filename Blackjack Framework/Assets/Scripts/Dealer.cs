﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dealer : MonoBehaviour
{
    [Header("Script References")]
    [SerializeField] private GameManager gM;
    [SerializeField] private UIManager interfaceManager;
    // [SerializeField] private Player player;


    [Header("Dealer Options")]
    [SerializeField] float payOut = 1.5f;
    [SerializeField] GameObject spawnLocatorDealer;
    [SerializeField] private Vector3 dealerOffset;
    [SerializeField] private float timeBetweenDraws;
    [SerializeField] private List<GameObject> dealersCards = new List<GameObject>();
    [SerializeField] private int dealersHand;

    private int counter = 0;
    private GameObject currentCard;
    private Vector3 nextSpawn;



    void Start()
    {
        nextSpawn = spawnLocatorDealer.transform.position;
        spawnLocatorDealer.SetActive(false);
    }

    public void DealerReset()
    {
        counter = 0;
        foreach (GameObject _cards in dealersCards)
        {
            Destroy(_cards);
        }

        nextSpawn = spawnLocatorDealer.transform.position;
        dealersCards.Clear();
    }

    public IEnumerator Deal() //Deals first 2 cards
    {
        yield return new WaitForSeconds(timeBetweenDraws);
        currentCard = gM.DrawACard(nextSpawn, false);
        dealersCards.Add(currentCard);
        MakeThisCardUnchangeable(currentCard);
        dealersHand = gM.FindCardValue(currentCard);
        interfaceManager.UpdateDealerHand(dealersHand);
        nextSpawn += dealerOffset;

        yield return new WaitForSeconds(timeBetweenDraws);
        currentCard = gM.DrawACard(nextSpawn, true);
        dealersCards.Add(currentCard);
        MakeThisCardUnchangeable(currentCard);
        dealersHand = gM.FindCardValue(currentCard);
        gM.AddPlayer(true, Vector3Int.zero);
        nextSpawn += dealerOffset;
    }

    public IEnumerator DealTo17()
    {
        yield return new WaitForSeconds(timeBetweenDraws);

        if (dealersHand < 17)
        {
            currentCard = gM.DrawACard(nextSpawn, false);
            nextSpawn += dealerOffset;
            dealersCards.Add(currentCard);
            MakeThisCardUnchangeable(currentCard);
            dealersHand += gM.FindCardValue(currentCard);
            interfaceManager.UpdateDealerHand(dealersHand);

            StartCoroutine(DealTo17());
        }
        else
        {
            gM.EvaluateAllHands(dealersHand);
        }
    }

    public IEnumerator FlipCard()
    {
        yield return new WaitForSeconds(timeBetweenDraws);
        dealersCards[1].GetComponent<CardHandleAI>().FlipAround();
        interfaceManager.UpdateDealerHand(dealersHand);
    }

    public void MakeThisCardUnchangeable(GameObject _card)
    {
        _card.GetComponent<CardHandleAI>().DisableAceChanging();
    }

}
